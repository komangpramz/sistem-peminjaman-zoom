<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomZoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_zooms', function (Blueprint $table) {
            $table->id();
            $table->string('meeting_id', 16);
            $table->string('topik');
            $table->time('jam_mulai');
            $table->time('jam_selesai');
            $table->string('passcode');
            $table->unsignedBigInteger('id_akun');            
            $table->foreign('id_akun')->references('id')->on('akun_zooms')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('id_peminjaman');            
            $table->foreign('id_peminjaman')->references('id')->on('peminjamans')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_zooms');
    }
}
