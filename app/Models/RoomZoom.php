<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomZoom extends Model
{
    use HasFactory;
    protected $guarded = [];

    // Relationship
    public function akun_zoom(){
        return $this->belongsTo(AkunZoom::class, 'id_akun');
    }

    public function peminjaman(){
        return $this->belongsTo(Peminjaman::class, 'id_peminjaman');
    }
}
