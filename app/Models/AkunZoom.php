<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AkunZoom extends Model
{
    use Softdeletes;
    
    use HasFactory;
    protected $guarded = [];

    // Relationship
    public function room_zoom(){
        return $this->hasMany(RoomZoom::class);
    }

    public function peminjaman(){
        return $this->hasMany(Peminjaman::class);
    }
}
