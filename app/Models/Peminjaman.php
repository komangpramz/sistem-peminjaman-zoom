<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    use HasFactory;
    protected $table = 'peminjamans';
    protected $guarded = [];

    // Relationship
    public function user(){
        return $this->belongsTo(User::class, 'id_user');
    }

    public function akun_zoom(){
        return $this->belongsTo(AkunZoom::class, 'id_akun');
    }

    public function room_zoom(){
        return $this->hasOne(RoomZoom::class);
    }
}