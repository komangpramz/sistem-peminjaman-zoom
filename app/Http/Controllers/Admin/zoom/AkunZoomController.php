<?php

namespace App\Http\Controllers\Admin\zoom;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AkunZoom;
use App\Models\RoomZoom;



class AkunZoomController extends Controller
{
    public function viewData(){
        $az = AkunZoom::all();

        return view ('admin.akun-zoom.viewAkun',["akun_zoom"=>$az]);
    }

    public function simpanData(Request $request){
        $request->validate([
            'nama' => 'required|max:255',
            'kapasitas' => 'required'
        ]);

        $az = new akunzoom();
        $az->nama_akun = $request->nama;
        $az->kapasitas = $request->kapasitas;
        $az->status = 1;

        $az->save();

        return redirect('/admin/zoom')->with('success', 'Akun Zoom Berhasil Ditambah!');

    }

    public function editData($id){
        $az = AkunZoom::where('id', $id)->first();

        return view ('admin.akun-zoom.editAkun',["akun_zoom"=>$az]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required|max:255',
            'kapasitas' => 'required',
            'status' => 'required'
        ]);

        $az = AkunZoom::find($id);

        $az->nama_akun = $request->nama;
        $az->kapasitas = $request->kapasitas;
        $az->status = $request->status;

        $az->save();

        return redirect('/admin/zoom')->with('success', 'Akun Zoom Berhasil Diupdate!');
    }

    public function delete($id){
        $az = AkunZoom::findOrFail($id);

        $az->delete();
        return redirect('/admin/zoom')->with('success', 'Akun Zoom Berhasil Dihapus!');
    }

    public function viewRooms($id){
        $room = RoomZoom::where('id_akun', $id)->get();
        $akun = AkunZoom::where('id', $id)->first();

        return view('admin.akun-zoom.viewRooms', [
            "rooms" => $room,
            "akun" => $akun
        ]);

    }

    public function editRooms($id){
        $room = RoomZoom::where('id', $id)->first();

        return view('admin.akun-zoom.editRooms', [
            "room" => $room        
        ]);
    }

    public function updateRooms(Request $request, $id){
        $request->validate([
            'topik' => 'required',
            'passcode' => 'required|min:6|max:6'
        ]);

        $room = RoomZoom::findOrFail($id);

        $room->topik = $request->topik;
        $room->passcode = $request->passcode;        

        $room->save();

        return redirect('/admin/zoom/')->with('success', 'Room Zoom Berhasil Diupdate!');
    }

    public function trash()
    {
        $az = AkunZoom::onlyTrashed()->paginate(10);
        return view('admin.akun-zoom.trash', ['akun_zoom' => $az]);
    }

    public function restore($id)
    {
        $az = AkunZoom::withTrashed()->findOrFail($id);
        if($az->trashed()){            
            $az->restore();
            
            return redirect('admin/zoom/trash')->with('success', 'Akun Zoom Berhasil Dikembalikan!');
        
        }
    } 

    public function deletePermanent($id)
    {
        $az = AkunZoom::withTrashed()->findOrFail($id);
        if($az->trashed())
        {
            $az->forceDelete();
            return redirect('admin/zoom/trash')->with('success', 'Akun Zoom Berhasil Dihapus Permanen!');
        }
    } 
}
