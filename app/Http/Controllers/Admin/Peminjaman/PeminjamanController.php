<?php

namespace App\Http\Controllers\Admin\Peminjaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Peminjaman;
use App\Models\User;
use App\Models\RoomZoom;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;



class PeminjamanController extends Controller
{
    public function viewData(){
        $data = Peminjaman::all();
                
        return view('admin.peminjaman.viewPeminjaman',
            [
                "peminjaman" => $data
            ]
        );        
    }

    public function editData($id) {
        $data = Peminjaman::where('id',$id)->first();

        return view ('admin.peminjaman.editPeminjaman',["peminjaman"=>$data]);

    }    

    public function updateData(Request $request){        
        $data = Peminjaman::find($request->id);

        $request->validate([
            'status' => 'required'
        ]);
        
        // Pengecekan jika status Disetujui
        if($request->status == "Disetujui") {
            // Inisialisasi model RoomZoom
            $room = new RoomZoom();

            $room->meeting_id = mt_rand(1000000000, 9999999999);
            $room->topik = $request->nama_kegiatan;
            $room->jam_mulai = $request->jam_mulai;
            $room->jam_selesai = $request->jam_selesai;
            $room->passcode = Str::random(6);
            $room->id_akun = $request->id_akun;
            $room->id_peminjaman = $request->id;

            // Menambahkan data Room Zoom
            $room->save();

        } else {
            // Jika status selain Disetujui

            // Mengambil data room zoom
            $room = RoomZoom::where('id_peminjaman', $request->id)->first(); 
            // Pengecekan apakah terdapat data
            if($room != NULL) {
                // Delete data room zoom jika terdapat data
                $room->delete();
            }
        }
                
        $data->status = $request->status;

        if($request->status == "Ditolak"){
            
            // Keterangan wajib diisi
            $request->validate(
                [
                    'keterangan' => 'required'
                ]
            );

            $data->keterangan = $request->keterangan;        
        } else {
            $data->keterangan = '';        
        }

        $data->save();

        return redirect('/admin/peminjaman')->with('success', 'Data Peminjaman Berhasil Diupdate!');
    }    
}
