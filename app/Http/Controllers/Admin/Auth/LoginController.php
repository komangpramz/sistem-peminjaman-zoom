<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('admin.auth.login');
    }

    public function authenticate(Request $request){

        // Validasi
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        // Mencoba autentikasi
        if(Auth::attempt($credentials)) {
            if(auth()->user()->role == 'Admin'){
                $request->session()->regenerate();
    
                return redirect()->intended(route('admin.dashboard'));
            } else {
                return back()->with('fail', 'Access Denied');

            }
        }

        // Gagal autentikasi
        return back()->with('fail', 'Login Gagal');

    }

}
