<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        return view('admin.auth.register');
    }

    public function store(Request $request){
        
        // Enkripsi
        $request->pass = Hash::make($request->pass);

        // Validasi
        $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'pass' => 'required|min:8'
        ]);            

        // Store Data
        User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => $request->pass,
            'role' => 'Admin'
        ]);   

        return redirect('admin/login')->with('success', 'Registrasi Berhasil, Silahkan Login Disini!');                
    }
}
