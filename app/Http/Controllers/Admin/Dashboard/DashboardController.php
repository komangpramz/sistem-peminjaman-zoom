<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Peminjaman;


class DashboardController extends Controller
{
    public function index(){
        $pending = Peminjaman::where('status', 'Diajukan')->count();
        $approved = Peminjaman::where('status', 'Disetujui')->count();
        $declined = Peminjaman::where('status', 'Ditolak')->count();
        $finish = Peminjaman::where('status', 'Selesai')->count();
        $total = Peminjaman::count();

        return view('admin.dashboard.index', [
            'pending' => $pending,
            'approved' => $approved,
            'declined' => $declined,
            'finish' => $finish,
            'total' => $total,
        ]);
    }
}
