<?php

namespace App\Http\Controllers\Admin\Jadwal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Peminjaman;
use App\Models\RoomZoom;
use Illuminate\Support\Facades\Auth;

class JadwalController extends Controller
{
    public function viewData(){
        $data = Peminjaman::where('status', 'Disetujui')->get();

        return view('admin.jadwal.viewJadwal', ["jadwal" => $data]);
    }

    public function detailJadwal($id){
        $data = RoomZoom::where('id_peminjaman', $id)->first();

        return view('admin.jadwal.detailJadwal', ["room" => $data]);
    }
}
