<?php

namespace App\Http\Controllers\Mahasiswa\Jadwal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Peminjaman;
use App\Models\RoomZoom;
use Illuminate\Support\Facades\Auth;

class JadwalController extends Controller
{
    public function viewData(){
        $id_user = Auth::id();
        $data = Peminjaman::where(['status' => 'Disetujui', 'id_user' => $id_user])->get();

        return view('mahasiswa.jadwal.viewJadwal', ["peminjaman" => $data]);
    }

    public function detailJadwal($id){
        $data = RoomZoom::where('id_peminjaman', $id)->first();

        return view('mahasiswa.jadwal.detailJadwal', ["room" => $data]);
    }
}
