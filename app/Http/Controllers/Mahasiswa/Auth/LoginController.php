<?php

namespace App\Http\Controllers\Mahasiswa\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('mahasiswa.auth.login');
    }

    public function authenticate(Request $request){

        // Validasi
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'            
        ]);

        // Mencoba autentikasi
        if(Auth::attempt($credentials)) {           
            if(auth()->user()->role == 'Mahasiswa'){
                $request->session()->regenerate();
    
                return redirect()->intended(route('mahasiswa.dashboard'));
            } else {
                return back()->with('fail', 'Access Denied');

            }
        }

        // Gagal autentikasi
        return back()->with('fail', 'Login Gagal');

    }
}
