<?php

namespace App\Http\Controllers\Mahasiswa\Peminjaman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Peminjaman;
use App\Models\AkunZoom;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PeminjamanController extends Controller
{
    public function viewData(){
        $id_user = Auth::id();
        $pz = Peminjaman::where('id_user', $id_user)->get();
        
        return view ('mahasiswa.peminjaman.viewPinjaman',["peminjaman"=>$pz]);
    }

    public function tambahData(){
        $zoom = AkunZoom::where('status', 1)->get();

        return view ('mahasiswa.peminjaman.AddPinjaman', ["zoom"=>$zoom]);
    }

    public function simpanData(Request $request){   
        // Validasi
        $request->validate(
            [
                'nama' => 'required|max:255',
                'deskripsi' => 'required',
                'tanggal' => 'required|date',
                'mulai' => 'required|before:selesai',
                'selesai' => 'required|after:mulai',
                'id_akun' => 'required'
            ],
            [
                'id_akun.required' => 'Akun Zoom must be selected.'
            ]
        );
        
        // Pengecekan Peminjaman                
        $start = Carbon::parse($request->mulai);        
        $end = Carbon::parse($request->selesai);

        // Pengambilan data peminjaman dari database berdasarkan akun zoom yang dipilih mahasiswa
        $peminjaman = Peminjaman::where(['id_akun' => $request->id_akun, 'tanggal_kegiatan' => $request->tanggal])->get();        

        // Looping Data Peminjaman
        foreach($peminjaman as $p){
            // Set Waktu Mulai dan Waktu Akhir Dari Database
            $start_time = Carbon::parse($p->jam_mulai);
            $end_time = Carbon::parse($p->jam_selesai);    

            // Kondisi Jika Waktu Peminjaman Berada Pada Waktu Yang Sudah Ada
            if($start->between($start_time,$end_time,true) || $end->between($start_time,$end_time,true || $start_time->between($start, $end, true))){
                return redirect('/mahasiswa/peminjaman')->with('fail', 'Akun Zoom Sudah Dipinjam Pada Waktu Tersebut!');
            }            
        }

        // Inisialisasi Peminjaman Baru
        $pz = new Peminjaman();
        $pz->nama_kegiatan = $request->nama;
        $pz->deskripsi = $request->deskripsi;
        $pz->tanggal_kegiatan = $request->tanggal;
        $pz->jam_mulai = $request->mulai;
        $pz->jam_selesai = $request->selesai;
        $pz->status = "Diajukan";
        $pz->id_akun = $request->id_akun;
        $pz->id_user = Auth::id();

        // Insert Peminjaman
        $pz->save();

        // Return With Message
        return redirect('/mahasiswa/peminjaman')->with('success', 'Peminjaman Berhasil Diajukan!');

    }
    
    public function delete($id){
        $pz = Peminjaman::findOrFail($id);
        
        $pz->delete();
        return redirect('/mahasiswa/peminjaman')->with('success', 'Peminjaman Berhasil Dihapus!');
    }   
}

