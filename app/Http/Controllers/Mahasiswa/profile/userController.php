<?php

namespace App\Http\Controllers\Mahasiswa\profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class userController extends Controller
{
    public function viewProfile(){
        $id = Auth::id();
        $u = User::where ('id',$id)->get();

        return view ('mahasiswa.profile.profile',["user"=>$u]);
    }
    public function editProfile($id){
        $u = User::where('id',$id)->first();

        return view ('mahasiswa.profile.editProfile',["user"=>$u]);
    }
    public function update(Request $request, $id){

        // Validasi
        $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email',
            'no_induk' => 'max:16',
            'jenis_kelamin' => 'required'
        ]);

        $u = User::find($id);

        $u->email = $request->email;
        $u->nama = $request->nama;
        $u->nomor_induk = $request->no_induk;
        $u->alamat = $request->alamat;
        $u->jenis_kelamin = $request->jenis_kelamin;

        $u->save();

        return redirect('/mahasiswa/profile')->with('success', 'Data Profil Berhasil Diupdate!');
    }
}
