<?php

namespace App\Http\Controllers\Mahasiswa\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Peminjaman;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    public function index(){
        $id_user = Auth::id();
        $pending = Peminjaman::where('status', 'Diajukan')->where('id_user', $id_user)->count();
        $approved = Peminjaman::where('status', 'Disetujui')->where('id_user', $id_user)->count();
        $declined = Peminjaman::where('status', 'Ditolak')->where('id_user', $id_user)->count();
        $finish = Peminjaman::where('status', 'Selesai')->where('id_user', $id_user)->count();
        $total = Peminjaman::where('id_user', $id_user)->count();

        return view('mahasiswa.dashboard.index', [
            'pending' => $pending,
            'approved' => $approved,
            'declined' => $declined,
            'finish' => $finish,
            'total' => $total,
        ]);
    }
}
