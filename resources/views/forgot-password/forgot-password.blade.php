<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Sistem Peminjaman Zoom</title>
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css'>
	<link rel="stylesheet" href="assets/css/style.css">

</head>

<body>
	<div class="container" id="container">
        <div class="overlay-container">
            <br><br><br><br><br>
			<center>
            <img src="assets/img/ilustration-white.png" alt="ilustrasi" >
            </center>
		</div>
		<div class="form-container sign-in-container">
            
            <form action="#" method="POST">
				<h1>Forgot Password</h1>
                <br>
				<span>Setel Ulang Password Anda</span>
                <br>
				<input type="email" placeholder="Email" />
				<input type="password" placeholder="New Password" />
				<br>
				<button>Submit</button>
                
			</form>
		</div>
		
	</div>
	<!-- partial -->
	<script src="assets/js/script.js"></script>

</body>

</html>