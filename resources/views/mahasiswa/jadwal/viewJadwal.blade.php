@extends('master.app2')
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <span>Jadwal Peminjaman </span>
        <br>
        <br>
        <br>
        <table id="zero-config" class="table dt-table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Nama Kegiatan</th>
                    <th>Deskripsi</th>
                    <th>Tanggal</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesai</th>
                    <th>Akun Zoom</th>
                    <th>Status</th>
                    <th class="no-content">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($peminjaman as $p)
                <tr>
                    <td>{{ $p->nama_kegiatan }}</td>
                    <td>{{ $p->deskripsi }}</td>
                    <td>{{date('d M Y', strtotime($p->tanggal_kegiatan))}}</td>
                    <td>{{ $p->jam_mulai }}</td>
                    <td>{{ $p->jam_selesai }}</td>
                    <td>{{ $p->akun_zoom->nama_akun }}</td>
                    <td>
                        <span class="badge badge-success"> {{ $p->status }} </span>
                    </td>
                    <td>
                        <a href="/mahasiswa/jadwal/detail/{{$p->id}}"><u> Detail</u></a>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="8" style="color: red; text-align:center">Data Jadwal Kosong</td>
                    </tr> 
                @endforelse                
            </tbody>
        </table>
    </div>
</div>

<div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All rights
            reserved.</p>
    </div>
    <div class="footer-section f-section-2">
        <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-heart">
                <path
                    d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                </path>
            </svg></p>
    </div>
</div>
@endsection
