@extends('master.app2')
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <span>Ajukan Peminjaman Akun Zoom</span>
        <br><br><br>
        <form action="/mahasiswa/peminjaman/simpanPeminjaman" method="POST">
            {{ csrf_field() }}
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Nama Kegiatan</label>
                <input value="{{ old('nama') }}" type="text" name="nama" class="form-control" placeholder="Masukkan Nama Kegiatan...">
                @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Deskripsi Kegiatan</label>
                <textarea name="deskripsi" class="form-control" placeholder="Masukkan Deskripsi Kegiatan..."></textarea>
                @error('deskripsi')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Tanggal</label>
                <input type="date" name="tanggal" class="form-control" placeholder="Masukkan Tanggal Kegiatan...">
                @error('tanggal')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Jam Mulai</label>
                <input value="{{ old('mulai') }}" type="time" name="mulai" class="form-control" placeholder="Masukkan Jam Mulai Kegiatan...">
                @error('mulai')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Jam Selesai</label>
                <input type="time" value="{{ old('selesai') }}" name="selesai" class="form-control" placeholder="Masukkan Jam Selesai Kegiatan...">
                @error('selesai')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label for="fakultas">Akun Zoom</label>
                <select class="form-control" name="id_akun" id="id_akun">
                    <option selected disabled>Pilih Akun Zoom Yang Ingin Dipinjam...</option>
                    @foreach ($zoom as $z)
                    <option value="{{$z->id}}">{{$z->nama_akun}} - {{$z->kapasitas}} Partisipan</option>
                    @endforeach
                </select>
                @error('id_akun')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <br>
            <input type="submit" name="submit" class="mt-4 mb-4 btn btn-primary">
        </form>
    </div>
</div>
@endsection
