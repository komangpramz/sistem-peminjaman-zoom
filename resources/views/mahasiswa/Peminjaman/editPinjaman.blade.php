@extends('master.app2')
@section('content')
    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div class="widget-content widget-content-area br-6">
            <span>Edit Data Peminjaman Akun Zoom</span>
            <br><br><br>
            <form action="/mahasiswa/peminjaman/updateData/{{$peminjaman->id}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Nama Kegiatan</label>
                    <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Kegiatan...">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Deskripsi Kegiatan</label>
                    <textarea name="deskripsi" class="form-control" placeholder="Masukkan Deskripsi Kegiatan..."></textarea>
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Tanggal</label>
                    <input type="date" name="tanggal" class="form-control" placeholder="Masukkan Tanggal Kegiatan...">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Jam Mulai</label>
                    <input type="time" name="mulai" class="form-control" placeholder="Masukkan Jam Mulai Kegiatan...">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Jam Selesai</label>
                    <input type="time" name="selesai" class="form-control" placeholder="Masukkan Jam Selesai Kegiatan...">
                </div>  
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Status Aktif</label>
                    <input type="number" name="status" class="form-control" id="exampleFormControlInput2" placeholder="Masukkan Status Aktif Akun Zoom...">
                </div> 
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Id akun</label>
                    <input type="text" name="id_akun" class="form-control" id="exampleFormControlInput2" placeholder="Masukkan Status Aktif Akun Zoom...">
                </div>                
                <input type="submit" name="submit" class="mt-4 mb-4 btn btn-primary">
            </form>
        </div>
    </div>
@endsection