@extends('master.app2')
@section('custom-css')
    <link href="{{ asset('assets/css/tables/table-basic.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    {{-- ALERT --}}
    @if (Session::has('success'))
    <div class="alert alert-light-success border-0 mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg> ... </svg></button>
        <strong>{{ Session::get('success') }}</strong></button>
    </div>
    @endif
    @if (Session::has('fail'))
    <div class="alert alert-light-danger border-0 mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg> ... </svg></button>
        <strong>{{ Session::get('fail') }}</strong></button>
    </div>
    @endif
    <div class="widget-content widget-content-area br-6">
        <span>Data Peminjaman Akun Zoom</span>
        <br>
        <br>
        <table id="zero-config" class="table dt-table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Nama Kegiatan</th>
                    <th>Deskripsi</th>
                    <th>Tanggal</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesai</th>
                    <th>Status</th>
                    <th class="no-content">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($peminjaman as $pz)
                    <tr>
                        <td>{{$pz->nama_kegiatan}}</td>
                        <td>{{$pz->deskripsi }}</td>
                        <td>{{date('d M Y', strtotime($pz->tanggal_kegiatan))}}</td>
                        <td>{{$pz->jam_mulai}}</td>
                        <td>{{$pz->jam_selesai}}</td>
                        <td>
                            @if ($pz->status == "Diajukan")
                            <span class="badge badge-info"> {{ $pz->status }} </span>
                            @elseif ($pz->status == "Disetujui")
                            <span class="badge badge-success"> {{ $pz->status }} </span>
                            @elseif ($pz->status == "Ditolak")
                            <span class="badge badge-danger"> {{ $pz->status }} </span>
                            @elseif ($pz->status == "Selesai")
                            <span class="badge badge-secondary"> {{ $pz->status }} </span>
                            @endif
                        </td>
                        <td style="text-align: center">   
                            @if ($pz->status != "Disetujui"  && $pz->status != "Selesai")
                            <ul class="table-controls">
                                <li><a href="/mahasiswa/peminjaman/delete/{{$pz->id}}" onclick="return confirm('Apakah anda yakin menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 text-danger"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></a></li>
                            </ul>                            
                            @else 
                            -
                            @endif                                            
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" style="color: red; text-align:center">Data Peminjaman Kosong</td>
                    </tr> 
                @endforelse
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
<div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All rights
            reserved.</p>
    </div>
    <div class="footer-section f-section-2">
        <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-heart">
                <path
                    d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                </path>
            </svg></p>
    </div>
</div>
<!-- END MAIN CONTAINER -->
@endsection
@section('custom-script')
<script src="assets/js/scrollspyNav.js"></script>
<script>
    checkall('todoAll', 'todochkbox');
    $('[data-toggle="tooltip"]').tooltip()
</script>
@endsection
