@extends('master.app2')
@section('custom-css')
    <link href="{{ asset('assets/css/users/user-profile.css') }}" rel="stylesheet" type="text/css" />
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/elements/alert.css') }}">
    <link href="{{ asset('assets/css/tables/table-basic.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .btn-light {
            border-color: transparent;
        }

    </style>
    <!--  END CUSTOM STYLE FILE  -->
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    {{-- ALERT --}}
    @if (Session::has('success'))
    <div class="alert alert-light-success border-0 mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg> ... </svg></button>
        <strong>{{ Session::get('success') }}</strong></button>
    </div>
    @endif
    @if (Session::has('fail'))
    <div class="alert alert-light-danger border-0 mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg> ... </svg></button>
        <strong>{{ Session::get('fail') }}</strong></button>
    </div>
    @endif
    <div class="widget-content widget-content-area br-6">
        <span>Profile Saya</span>
        <br>
        @foreach ($user as $u)
        <div class="d-flex flex-row-reverse">
            <a href="/mahasiswa/profile/edit-profile/{{$u->id}}" button class="btn btn-primary mb-2 mr-2"> <i
                    class="mdi mdi-pen" style="color: white"></i> Edit Profile</button></a>
        </div>
        <div class="mb-3 row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <span class="form-control-plaintext">{{$u->email}}</span>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="staticNama" class="col-sm-2 col-form-label">Nama Lengkap</label>
            <div class="col-sm-10">
                <span class="form-control-plaintext">{{$u->nama}}</span>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="staticNIP" class="col-sm-2 col-form-label">NIM</label>
            <div class="col-sm-10">
                <span class="form-control-plaintext">{{$u->nomor_induk}}</span>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="staticGender" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-10">
                @if ($u->jenis_kelamin == "L")
                    <span class="form-control-plaintext">Laki-Laki</span>
                @elseif ($u->jenis_kelamin == "P")
                    <span class="form-control-plaintext">Perempuan</span>
                @else 
                    
                @endif
            </div>
        </div>
        <div class="mb-3 row">
            <label for="staticAlamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-10">
                <p class="form-control-plaintext">{{$u->alamat}}</p>
            </div>
        </div>
        @endforeach
    </div>

</div>
</div>
@endsection
