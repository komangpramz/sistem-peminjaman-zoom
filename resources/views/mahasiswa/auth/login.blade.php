<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Sistem Peminjaman Zoom</title>
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css'>
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

</head>

<body>

    {{-- ALERT --}}
    @if (Session::has('success'))
        <div class="alert success-message">
            {{ Session::get('success') }}    
        </div>        
    @endif
    @if (Session::has('fail'))
    <div class="alert failed-message">
        {{ Session::get('fail') }}    
    </div>        
@endif

	<div class="container" id="container">        
        <div class="overlay-container">
            <br><br><br><br><br>
			<center>
            <img src="{{ asset('assets/img/ilustration-white.png') }}" alt="ilustrasi" >
            </center>
		</div>
		<div class="form-container sign-in-container">
            
            <form action="login" method="POST">
				<h1>Login</h1>
				@csrf
                <br>
				<span>Masukkan Akun Anda</span>
                <br>

				<input type="email" placeholder="Email" name="email" required value="{{ old('email') }}"/>
                @error('email')
                <span class="error-message">
                    {{ $message }}
                </span>                    
                @enderror

				<input type="password" placeholder="Password" name="password" required value="{{ old('pass') }}"/>
                @error('pass')
                <span class="error-message">
                    {{ $message }}
                </span>                    
                @enderror

				{{-- <a href="forgot-password">Lupa Kata Sandi?</a> --}}
				<button>Masuk</button>
                <br>
                <span>Belum punya akun ? <a href="{{ route('mahasiswa.register') }}">Buat akun</a></span>
			</form>
		</div>
		
	</div>
	<!-- partial -->
	<script src="{{ asset('assets/js/script.js') }}"></script>

</body>

</html>