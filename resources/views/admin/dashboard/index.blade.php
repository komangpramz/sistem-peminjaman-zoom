@extends('master.app')
@section('content')

<div class="col-xl-5 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
    <div class="widget widget-three">
        <div class="widget-heading">
            <h5 class="">Total Peminjaman</h5>            

        </div>
        <div class="widget-content">

            <div class="order-summary">

                <div class="summary-list">                   
                    <div class="w-summary-details">
                        
                        <div class="w-summary-info">
                            <h6>Pending</h6>
                            <p class="summary-count text-info">{{$pending}} Peminjaman</p>
                        </div>                        
                    </div>
                </div>
                <div class="summary-list">                   
                    <div class="w-summary-details">
                        
                        <div class="w-summary-info">
                            <h6>Disetujui</h6>
                            <p class="summary-count text-success">{{$approved}} Peminjaman</p>
                        </div>                        
                    </div>
                </div>
                <div class="summary-list">                   
                    <div class="w-summary-details">
                        
                        <div class="w-summary-info">
                            <h6>Ditolak</h6>
                            <p class="summary-count text-danger">{{$declined}} Peminjaman</p>
                        </div>                        
                    </div>
                </div>
                <div class="summary-list">                   
                    <div class="w-summary-details">
                        
                        <div class="w-summary-info">
                            <h6>Selesai</h6>
                            <p class="summary-count text-secondary">{{$finish}} Peminjaman</p>
                        </div>                        
                    </div>
                </div>  
                <hr>
                <div class="summary-list">                   
                    <div class="w-summary-details">
                        
                        <div class="w-summary-info">
                            <strong>TOTAL</strong>
                            <strong class="summary-count text-primary">{{$total}} Peminjaman</strong>
                        </div>                        
                    </div>
                </div>                            
            </div>            
        </div>
    </div>
</div>

<div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <p class="">Ujian Akhir Semester Pemrograman Berbasis Framework</p>
    </div>
    <div class="footer-section f-section-2">
        <p class="">Coded with 
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>
        </p>
    </div>
</div>
@endsection
    
        