@extends('master.app')
@section('content')
    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div class="widget-content widget-content-area br-6">
            <span>Tambah Data Akun Zoom</span>
            <br><br><br>
            <form action="/admin/zoom/simpanAkunZoom" method="POST">
                {{ csrf_field() }}
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Nama Akun</label>
                    <input type="text" name="nama" class="form-control" id="exampleFormControlInput2" placeholder="Masukkan Nama Akun Zoom...">
                    @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Kapasitas</label>
                    <select name="kapasitas" class="form-control">
                        <option disabled selected>Silahkan Pilih Kapasitas Akun</option>
                        <option value="100">100 Partisipan</option>
                        <option value="300">300 Partisipan</option>
                        <option value="500">500 Partisipan</option>
                        <option value="1000">1000 Partisipan</option>
                    </select>
                    @error('kapasitas')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>             
                <input type="submit" name="time" class="mt-4 mb-4 btn btn-primary">
            </form>
        </div>
    </div>
@endsection