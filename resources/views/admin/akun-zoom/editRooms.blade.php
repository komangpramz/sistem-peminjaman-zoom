@extends('master.app')
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <span>Edit Data Room Zoom</span>
        <br><br><br>
        <form action="/admin/zoom/rooms/update/{{$room->id}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group mb-4">
                <label>MEETING ID</label>
                <input required  readonly type="text" name="meeting_id" class="form-control" id="exampleFormControlInput2"
                    value="{{$room->meeting_id}}">                
            </div>
            <div class="form-group mb-4">
                <label>TOPIK</label>
                <input required type="text" name="topik" class="form-control" id="exampleFormControlInput2"
                    value="{{$room->topik}}">
                @error('topik')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label>PASSCODE</label>
                <input required type="text" name="passcode" class="form-control" id="exampleFormControlInput2"
                    value="{{$room->passcode}}">
                @error('passcode')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>            
            <input type="submit" name="time" class="mt-4 mb-4 btn btn-primary">
        </form>
    </div>
</div>
@endsection
