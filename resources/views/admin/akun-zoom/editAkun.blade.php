@extends('master.app')
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <span>Edit Data Akun Zoom</span>
        <br><br><br>
        <form action="/admin/zoom/updateData/{{$akun_zoom->id}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Nama Akun</label>
                <input type="text" name="nama" class="form-control" id="exampleFormControlInput2"
                    value="{{$akun_zoom->nama_akun}}">
                @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Kapasitas</label>
                <select  name="kapasitas" class="form-control">
                    <option disabled>Silahkan Pilih Kapasitas Akun</option>
                    <option {{$akun_zoom->kapasitas == 100 ? 'selected' : ''}} value="100">100 Partisipan</option>
                    <option {{$akun_zoom->kapasitas == 300 ? 'selected' : ''}} value="300">300 Partisipan</option>
                    <option {{$akun_zoom->kapasitas == 500 ? 'selected' : ''}} value="500">500 Partisipan</option>
                    <option {{$akun_zoom->kapasitas == 1000 ? 'selected' : ''}} value="1000">1000 Partisipan</option>
                </select>      
                @error('kapasitas')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror         
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Status Aktif</label>
                <select name="status" class="form-control">
                    <option disabled>Silahkan Pilih Status Akun</option>
                    <option {{$akun_zoom->status == 1 ? 'selected' : ''}} value="1">Aktif</option>
                    <option {{$akun_zoom->status == 0 ? 'selected' : ''}} value="0">Non Aktif</option>
                </select>  
                @error('status')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror                  
            </div>
            <input type="submit" name="time" class="mt-4 mb-4 btn btn-primary">
        </form>
    </div>
</div>
@endsection
