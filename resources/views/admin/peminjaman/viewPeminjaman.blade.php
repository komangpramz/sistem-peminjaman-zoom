@extends('master.app')
@section('custom-css')
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/elements/alert.css') }}">
    <link href="{{ asset('assets/css/tables/table-basic.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .btn-light { border-color: transparent; }
    </style>
    <!--  END CUSTOM STYLE FILE  -->
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    {{-- ALERT --}}
    @if (Session::has('success'))
    <div class="alert alert-light-success border-0 mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg> ... </svg></button>
        <strong>{{ Session::get('success') }}</strong></button>
    </div>
    @endif
    @if (Session::has('fail'))
    <div class="alert alert-light-danger border-0 mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg> ... </svg></button>
        <strong>{{ Session::get('fail') }}</strong></button>
    </div>
    @endif
    <div class="widget-content widget-content-area br-6">
        <span>Peminjaman Akun Zoom</span>
        <br><br><br>
        <table id="zero-config" class="table dt-table-hover" style="width:100%">
            <thead>
                <tr>
                    <th>Nama Kegiatan</th>
                    <th>Deskripsi</th>
                    <th>Tanggal</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesai</th>
                    <th>Status</th>
                    <th>Nama Akun</th>
                    <th>Nama Peminjam</th>
                    <th class="no-content">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($peminjaman as $p)
                <tr>
                    <td>{{$p->nama_kegiatan}}</td>
                    <td>{{$p->deskripsi}}</td>
                    <td>{{date('d M Y', strtotime($p->tanggal_kegiatan))}}</td>
                    <td>{{$p->jam_mulai}}</td>
                    <td>{{$p->jam_selesai}}</td>
                    <td>
                        @if ($p->status == "Diajukan")
                        <span class="badge badge-info"> {{ $p->status }} </span>
                        @elseif ($p->status == "Disetujui")
                        <span class="badge badge-success"> {{ $p->status }} </span>
                        @elseif ($p->status == "Ditolak")
                        <span class="badge badge-danger"> {{ $p->status }} </span>
                        @elseif ($p->status == "Selesai")
                        <span class="badge badge-secondary"> {{ $p->status }} </span>
                        @endif
                    </td>
                    <td>{{$p->akun_zoom->nama_akun}}</td>
                    <td>{{$p->user->nama}}</td>
                    <td style="text-align: center">
                        @if ($p->status != "Selesai")
                        <ul class="table-controls">
                            <li><a href="/admin/peminjaman/edit/{{$p->id}}"  data-toggle="tooltip" data-placement="top" title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 text-success"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></a></li>                            
                        </ul>                         
                        @else
                        -
                        @endif

                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="9" style="color: red; text-align:center">Data Peminjaman Kosong</td>
                    </tr> 
                @endforelse
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
<div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All rights
            reserved.</p>
    </div>
    <div class="footer-section f-section-2">
        <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-heart">
                <path
                    d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                </path>
            </svg></p>
    </div>
</div>
<!-- END MAIN CONTAINER -->
@endsection
@section('custom-script')
<script src="assets/js/scrollspyNav.js"></script>
<script>
    checkall('todoAll', 'todochkbox');
    $('[data-toggle="tooltip"]').tooltip()
</script>
@endsection
