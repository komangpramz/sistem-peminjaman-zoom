@extends('master.app')
@section('content')
    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div class="widget-content widget-content-area br-6">
            <span>Edit Data Peminjaman</span>
            <br><br><br>
            <form action="  {{route('admin.updatePeminjaman')}} " method="POST">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" name="id" value="{{$peminjaman->id}}">
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Nama Kegiatan</label>
                    <input type="text" class="form-control" name="nama_kegiatan" readonly value="{{$peminjaman->nama_kegiatan}}">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Deskripsi</label>
                    <textarea class="form-control"name="deskripsi" readonly>{{$peminjaman->deskripsi}}</textarea>
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Tanggal</label>
                    <input type="date" class="form-control"name="" readonly name="tanggal_kegiatan" value="{{$peminjaman->tanggal_kegiatan}}">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Jam Mulai </label>
                    <input type="time" class="form-control"name="jam_mulai" readonly value="{{$peminjaman->jam_mulai}}">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Jam Selesai</label>
                    <input type="time" class="form-control"name="jam_selesai" readonly value="{{$peminjaman->jam_selesai}}">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Nama Akun</label>
                    <input type="text" class="form-control" name="nama_akun" readonly value="{{$peminjaman->akun_zoom->nama_akun}}">
                </div>
                <input type="hidden" class="form-control" name="id_akun" readonly value="{{$peminjaman->akun_zoom->id}}">
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Nama Mahasiswa</label>
                    <input type="text" class="form-control" name="id_user" readonly value="{{$peminjaman->user->nama}}">
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Status</label>
                    <select name="status" class="form-control" id="status">
                        <option {{ $peminjaman->status == "Diajukan" ? 'selected' : '' }} value="Diajukan">Diajukan</option>
                        <option {{ $peminjaman->status == "Disetujui" ? 'selected' : '' }} value="Disetujui">Disetujui</option>
                        <option {{ $peminjaman->status == "Ditolak" ? 'selected' : '' }} value="Ditolak">Ditolak</option>
                        <option {{ $peminjaman->status == "Selesai" ? 'selected' : '' }} value="Selesai">Selesai</option>
                    </select>
                    @error('status')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group mb-4" id="keterangan">
                    <label for="exampleFormControlInput2">Catatan</label>
                    <textarea type="text" class="form-control"name="keterangan">{{ $peminjaman->keterangan }}</textarea>
                    @error('keterangan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <input type="submit" name="time" class="mt-4 mb-4 btn btn-primary">
            </form>
        </div>
    </div>
@endsection