@extends('master.app')
@section('content')
<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
    <div class="widget-content widget-content-area br-6">
        <span>Detail Jadwal Peminjaman </span>
        <br>
        <br>
        <br>
        <form>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Meeting ID</label>
                <input type="text" class="form-control" readonly value="{{ $room->meeting_id }}">
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Topik</label>
                <input type="text" class="form-control" readonly value="{{ $room->topik }}">
            </div>
            {{-- <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Tanggal</label>
                <input type="date" class="form-control" readonly value="{{ $room->tanggal }}">
            </div> --}}
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Jam Mulai </label>
                <input type="time" class="form-control" readonly value="{{ $room->jam_mulai }}">
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Jam Selesai</label>
                <input type="time" class="form-control" readonly value="{{ $room->jam_selesai }}">
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Passcode</label>
                <input type="text" class="form-control" readonly value="{{ $room->passcode }}">
            </div>
            <div class="form-group mb-4">
                <label for="exampleFormControlInput2">Nama Akun</label>
                <input type="text" class="form-control" readonly value="{{ $room->akun_zoom->nama_akun }}">
            </div>
        </form>
    </div>
</div>
<div class="footer-wrapper">
    <div class="footer-section f-section-1">
        <p class="">Copyright © 2021 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All rights
            reserved.</p>
    </div>
    <div class="footer-section f-section-2">
        <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-heart">
                <path
                    d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                </path>
            </svg></p>
    </div>
</div>
@endsection
