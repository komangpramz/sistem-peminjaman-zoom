<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Sistem Peminjaman Zoom</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css'>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

</head>

<body>
    <div class="container" id="container">
        <div class="overlay-container">
            <br><br><br><br><br>
            <center>
                <img src="{{ asset('assets/img/ilustration-white.png') }}" alt="ilustrasi">
            </center>
        </div>
        <div class="form-container sign-in-container">
            <form action="register" method="POST">
				@csrf
                <h1>Registrasi Pegawai</h1>				
                <br>
                <span>Buat Akun Anda</span>
                <br>

                <input value="{{@old('nama')}}" type="text" placeholder="Nama Lengkap" required name="nama" class="is-invalid"/>                                                
                @error('nama')
                <span class="error-message">
                    {{ $message }}
                </span>                    
                @enderror

                <input value="{{@old('email')}}" type="email" placeholder="Email" required name="email"/>
                @error('email')
                <span class="error-message">
                    {{ $message }}
                </span>                    
                @enderror

                <input type="password" placeholder="Password" required name="pass"/>
                @error('pass')
                <span class="error-message">
                    {{ $message }}
                </span>                    
                @enderror

                <button>Register</button>
                <br>
                <span>Sudah punya akun ? <a href="{{ route('admin.login') }}">Masuk</a></span>
            </form>
        </div>
    </div>
    <!-- partial -->
    <script src="{{ asset('assets/js/script.js') }}"></script>

</body>
