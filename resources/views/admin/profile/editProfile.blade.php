@extends('master.app')
@section('content')
    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        <div class="widget-content widget-content-area br-6">
            <span>Edit Profile</span>
            <br><br><br>
            <form action="/admin/profile/updateProfile/{{$user->id}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Email</label>
                    <input type="email" name="email" class="form-control" value="{{$user->email}}">
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Nama Lengkap</label>
                    <input type="text" name="nama"class="form-control" value="{{$user->nama}}">
                    @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">NIP</label>
                    <input type="number" name="no_induk" class="form-control" value="{{$user->nomor_induk}}">
                    @error('no_induk')
                    <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Alamat</label>
                    <input type="text" name="alamat"  class="form-control" value="{{$user->alamat}}">
                    @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlInput2">Jenis Kelamin</label>
                    <select name="jenis_kelamin" class="form-control">
                        <option disabled selected>Silahkan Pilih Jenis Kelamin</option>
                        <option {{$user->jenis_kelamin == 'L' ? 'selected' : ''}} value="L">Laki-Laki</option>
                        <option {{$user->jenis_kelamin == 'P' ? 'selected' : ''}} value="P">Perempuan</option>
                    </select> 
                    @error('jenis_kelamin')
                    <div class="invalid-feedback">
                        {{ $message }}
                      </div>
                    @enderror
                </div>
                             
                <input type="submit" name="submit" class="mt-4 mb-4 btn btn-primary">
            </form>
        </div>
    </div>
@endsection