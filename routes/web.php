<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->get('/', function () {
    return redirect('login');
});

// Mahasiswa Page
// Autentikasi Mahasiswa
Route::middleware('guest')->get('login', [App\Http\Controllers\Mahasiswa\Auth\LoginController::class,'index'])->name('mahasiswa.login');
Route::middleware('guest')->post('login', [App\Http\Controllers\Mahasiswa\Auth\LoginController::class,'authenticate']);
Route::middleware('guest')->get('register', [App\Http\Controllers\Mahasiswa\Auth\RegisterController::class,'index'])->name('mahasiswa.register');;
Route::middleware('guest')->post('register', [App\Http\Controllers\Mahasiswa\Auth\RegisterController::class,'store']);

 
// Halaman Mahasiswa
Route::prefix('mahasiswa')->middleware(['mahasiswaAuth', 'mahasiswa'])->group(function () {
    Route::get('/',[App\Http\Controllers\Mahasiswa\Dashboard\DashboardController::class,'index'])->name('mahasiswa.dashboard');

    // Halaman Profile
    Route::prefix('profile')->middleware(['mahasiswaAuth','mahasiswa'])->group(function(){
        Route::get('/',[App\Http\Controllers\Mahasiswa\profile\userController::class,'viewProfile']);
        Route::get('/edit-profile/{id}',[App\Http\Controllers\Mahasiswa\profile\userController::class,'editProfile']);
        Route::post('/updateProfile/{id}',[App\Http\Controllers\Mahasiswa\profile\userController::class,'update']);
    });

    // Halaman Peminjaman
    Route::prefix('peminjaman')->middleware(['mahasiswaAuth', 'mahasiswa'])->group(function(){
        Route::get('/',[App\Http\Controllers\Mahasiswa\Peminjaman\PeminjamanController::class,'viewData']);
        Route::post('/simpanPeminjaman',[App\Http\Controllers\Mahasiswa\Peminjaman\PeminjamanController::class,'simpanData']);
        Route::get('/delete/{id}',[App\Http\Controllers\Mahasiswa\Peminjaman\PeminjamanController::class,'delete']);
        Route::get('/AddPinjaman',[App\Http\Controllers\Mahasiswa\Peminjaman\PeminjamanController::class,'tambahData']);       
           
    });

    // Halaman Jadwal
    Route::prefix('jadwal')->middleware(['mahasiswaAuth', 'mahasiswa'])->group(function(){
        Route::get('/', [App\Http\Controllers\Mahasiswa\Jadwal\JadwalController::class, 'viewData']);
        Route::get('detail/{id}', [App\Http\Controllers\Mahasiswa\Jadwal\JadwalController::class, 'detailJadwal']);
    });
});


// Admin Page
// Autentikasi Admin
Route::middleware('guest')->get('admin/login', [App\Http\Controllers\Admin\Auth\LoginController::class,'index'])->name('admin.login');
Route::middleware('guest')->post('admin/login', [App\Http\Controllers\Admin\Auth\LoginController::class,'authenticate']);
Route::middleware('guest')->get('admin/register', [App\Http\Controllers\Admin\Auth\RegisterController::class,'index'])->name('admin.register');;
Route::middleware('guest')->post('admin/register', [App\Http\Controllers\Admin\Auth\RegisterController::class,'store']);

// Halaman Admin
Route::prefix('admin')->middleware(['adminAuth','admin'])->group(function () {
    Route::get('/',[App\Http\Controllers\Admin\Dashboard\DashboardController::class,'index'])->name('admin.dashboard');

    // Halaman Admin
    Route::prefix('profile')->middleware(['adminAuth','admin'])->group(function(){
        Route::get('/',[App\Http\Controllers\Admin\Profile\userController::class,'viewProfile']);
        Route::get('/edit-profile/{id}',[App\Http\Controllers\Admin\Profile\userController::class,'editProfile']);
        Route::post('/updateProfile/{id}',[App\Http\Controllers\Admin\Profile\userController::class,'update']);
    });

    // Halaman Akun Zoom
    Route::prefix('zoom')->middleware(['adminAuth','admin'])->group(function(){
        Route::get('/',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'viewData']);
        Route::post('/simpanAkunZoom',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'simpanData']);
        Route::get('/edit/{id}',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'editData']);
        Route::post('/updateData/{id}',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'update']);
        Route::get('/delete/{id}',[App\Http\Controllers\Admin\zoom\AkunZoomController::class,'delete']);
        Route::get('add-akun-zoom', function () {
            return view('admin.akun-zoom.addAkun');
        });
        Route::get('/rooms/{id}', [App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'viewRooms']);
        Route::get('/rooms/edit/{id}', [App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'editRooms']);
        Route::post('/rooms/update/{id}', [App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'updateRooms']);
        //Soft Deleting
        Route::get('/trash',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'trash']);
        Route::post('/restore/{id}',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'restore']);
        Route::delete('/deletepermanent/{id}',[App\Http\Controllers\Admin\Zoom\AkunZoomController::class,'deletePermanent']);
    });
    

    // Halaman Jadwal
    Route::prefix('jadwal')->middleware(['adminAuth','admin'])->group(function(){
        Route::get('/', [App\Http\Controllers\Admin\Jadwal\JadwalController::class,'viewData']);
        Route::get('detail/{id}', [App\Http\Controllers\Admin\Jadwal\JadwalController::class,'detailJadwal']);
    });

    // Halaman Peminjaman
    Route::prefix('peminjaman')->middleware(['adminAuth','admin'])->group(function(){
        Route::get('/',[App\Http\Controllers\Admin\Peminjaman\PeminjamanController::class,'viewData']);
        Route::get('edit/{id}', [App\Http\Controllers\Admin\Peminjaman\PeminjamanController::class,'editData']);
        Route::post('updateData', [App\Http\Controllers\Admin\Peminjaman\PeminjamanController::class,'updateData'])->name('admin.updatePeminjaman');
    });
});

// Logout
Route::post('logout', [App\Http\Controllers\Auth\LogoutController::class,'logout']);


